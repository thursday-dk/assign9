//********************************************************************
//File:	        Driver.java       
//Author:       Kalvin Dewey
//Date:	        12/2/17
//Course:       CPS100
//
//Problem Statement:
//  Rewrite the Sorting class so that both sorting algorithms put
//  the values in descending order. Create a driver class with a main
//  method to exercise the modifications.
//
//Inputs:	none
//Outputs:	results of exercise
// 
//********************************************************************
public class Driver
{

  public static void main(String[] args)
  {
    Integer[] toSort =
    {65638, 55803, 88019, 62988, 69911, 48112, 48289, 98066, 30157, 69446,
     64945, 2044, 12, 30980, 80361, 16161, 6331, 5673, 50698, 39056, 20};
    Integer[] toSort2 = toSort.clone();
    Sorting<Integer> sorts = new Sorting<Integer>();

    System.out.println("Inverted Selection Sort:");
    sorts.selectionSort(toSort);
    for (int i : toSort)
      System.out.println(i);
    System.out.println("Inverted Insertion Sort:");
    sorts.insertionSort(toSort2);
    for (int i : toSort2)
      System.out.println(i);
  }

}
